var express = require('express')
  , config = require('./config')
  , rendr = require('rendr')
  , compress = require('compression')
  , bodyParser = require('body-parser')
  , serveStatic = require('serve-static')
  , logger = require('morgan')
  , app = express();

app.use(compress());
app.use(serveStatic(__dirname + '/public'));
app.use(logger('combined'));
app.use(bodyParser.json());

var server = rendr.createServer({
  dataAdapterConfig: config
});

server.configure(function (expressApp) {
  app.use('/', expressApp);
});


function start(){
  var port = process.env.PORT || config.port || 3001;
  app.listen(port);
  console.log("server pid %s listening on port %s in %s mode",
    process.pid,
    port,
    app.get('env')
  );
}


/**
 * Only start server if this script is executed, not if it's require()'d.
 * This makes it easier to run integration tests on ephemeral ports.
 */
if (require.main === module) {
  start();
}

exports.app = app;
