// Target help: 
// http://gruntjs.com/configuring-tasks#globbing-patterns

	// Load modules
	var fs = require("fs");
	
	// Task details inside this function.
	module.exports = function(grunt) {
		var pkg = grunt.file.readJSON('package.json');
		
		grunt.initConfig({
			pkg: pkg,
			clean: {
				main: ['build/'],
				templates: ['build/js/templates.js']
			},
			copy: {
				main: {
					files: [
						{
							expand: true, 
							filter: 'isFile',
							cwd:	'assets/',
							src:  ['imgs/*', 'fonts/*'],
							dest: 'build/'
						},
						{
							src:  ['assets/index.html'],
							dest: 'build/index.html'
						},
						{
							src:  ['assets/layout.html'],
							dest: 'views/layout.html'
						}
					]
				}
			},
			uglify: {
				main: {
					options: {
						banner: '/* <%= grunt.template.today("dd-mm-yyyy") %> */\n'
					},
					files: {
						'build/js/libs/libs.js' : ['assets/vendor/*.js'],
						'build/js/main.js' : ['public/js/*.js']					
					}
				}
			},
			cssmin: {
				options: {
					banner: '/* <%= grunt.template.today("dd-mm-yyyy") %> */'
				},
				main: {
					files: {
						'build/css/main.css': ['public/css/*.css']
					}
				}
			},
			tree: {
				build: {
					options: {
						prettify: true,
						exclude: [
							'tree.json'
						],
					},
					files: {
						'build/tree.json'	: ['build/']
					}
				},
				public: {
					options: {
						prettify: true,
						exclude: [
							'tree.json'
						],
					},
					files: {
						'public/tree.json' : ['public/']
					}
				}
			},
			hashres: {
				options: {
					encoding:       'utf8',
					fileNameFormat: '${name}.${hash}.${ext}',
					renameFiles:    true // Changes on filesize.
				},
				main: {
					src: [
						// These files will be renamed!
						'build/js/*.js',
						'build/js/libs/*.js',
						'build/css/*.css',
						'build/imgs/*',
						'build/fonts/*'
					],
					dest: [
						// Files that link to above files and needs to be updated with the hashed name.
						'build/index.html',
						'build/css/*.css',
						'build/tree.json',
            'views/layout.html'
					],
				}
			}
		});
		
		// Load the modules
		grunt.loadNpmTasks('grunt-contrib-clean');
		grunt.loadNpmTasks('grunt-contrib-copy');
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-contrib-cssmin');
		grunt.loadNpmTasks('grunt-hashres');
		grunt.loadNpmTasks('grunt-tree');

		// Do the work.
		grunt.registerTask('build', [
			'clean:main',
			'copy',
			'uglify',
			'clean:templates',
			'cssmin',
			'tree',
			'hashres'
		]);

		grunt.registerTask('default', ['build'])
	};