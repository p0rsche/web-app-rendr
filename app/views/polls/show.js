var BaseView = require('../base');

module.exports = BaseView.extend({
  className: 'polls_show_view'
});
module.exports.id = 'polls/show';
