var BaseView = require('../base');

module.exports = BaseView.extend({
  className: 'pages_missing_view'
});
module.exports.id = 'pages/missing';
