var BaseAppView = require('rendr/client/app_view');
var $ = require('jquery');
var $body = $('body');


module.exports = BaseAppView.extend({
    initialize: function() {
        this.app.on('change:title', function(app, title) {
            document.title = title + ' | DecisionAgony';
        });
    }
});
