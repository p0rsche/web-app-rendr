var Base = require('./base');

module.exports = Base.extend({
    url: '/polls/:id',
    idAttribute: 'id'
});
module.exports.id = 'Poll';
