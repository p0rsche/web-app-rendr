module.exports = function(match) {
  match('',                   'pages#missing');
  match('polls/:id',          'polls#show');
};
