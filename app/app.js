var BaseApp = require('rendr/shared/app');

module.exports = BaseApp.extend({

  initialize: function() {

  },

  start: function() {
    BaseApp.prototype.start.call(this);
  },

  getAppViewClass: function() {
    return require('./views/app_view');
  }

});
